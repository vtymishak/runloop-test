//
//  FeedViewModel.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import Foundation

typealias ChangeLoadingStateCallback = (FeedViewModel) -> Void
typealias FeedUpdateCallback = ([[FeedItem]]?, ServerError?) -> Void

class FeedViewModel {
    // MARK: - Properties
    var feed: [[FeedItem]]
    
    var isLoadingInProgress: Bool = false
    
    var changeLoadingStateCallback: ChangeLoadingStateCallback?
    var feedUpdateCallback: FeedUpdateCallback?
    
    fileprivate var updateTimer: Timer?
    
    var hasLoadedItems: Bool {
        if feed.count > 1 {
            return feed[0].count > 0 || feed[1].count > 0
        } else if feed.count > 0 {
            return feed[0].count > 0
        } else {
            return false
        }
    }
    
    // MARK: - Init
    init() {
        feed = [[FeedItem]]()
        scheduleUpdate()
    }
    
    // MARK: - Public
    func request() {
        feedUpdateCallback?(nil, nil)
    }
    
    
    func registerChangeLoadingStateCallback(_ callback: @escaping ChangeLoadingStateCallback) {
        changeLoadingStateCallback = callback
    }
    
    func registerFeedUpdateCallback(_ callback: @escaping FeedUpdateCallback) {
        feedUpdateCallback = callback
    }
    
    var numberOfSections: Int {
        return feed.count
    }
    
    func numberOfRows(in section: Int) -> Int {
        guard feed.count > section else { return 0 }
        return feed[section].count
    }
    
    func feedItem(at indexPath: IndexPath) -> FeedItem? {
        guard feed.count > indexPath.section else { return nil }
        let subFeed = feed[indexPath.section]
        guard subFeed.count > indexPath.row else { return nil }
        return subFeed[indexPath.row]
    }
    
    func title(for section: Int) -> String {
        return ""
    }
    
    // MARK: - Helpers
    fileprivate func scheduleUpdate() {
        updateTimer?.invalidate()
        updateTimer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true, block: { [unowned self] (_) in
            self.changeLoadingStateCallback?(self)
            self.request()
        })
    }
}

extension Notification.Name {
    static let FeedItemSelected = Notification.Name("FeedItemSelected")
}
