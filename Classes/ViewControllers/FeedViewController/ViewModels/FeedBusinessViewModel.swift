//
//  FeedBusinessViewModel.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import Foundation

class FeedBusinessViewModel: FeedViewModel {
    // MARK: - Public
    override func request() {
        isLoadingInProgress = true
        API.requestBusinessFeed { [weak self] (items, error) in
            guard let `self` = self else { return }
            self.isLoadingInProgress = false
            guard error == nil else {
                self.feedUpdateCallback?(nil, error!)
                return
            }
            guard let items = items else {
                self.feedUpdateCallback?(nil, ServerError.common)
                return
            }
            self.feed = [items]
            self.feedUpdateCallback?(self.feed, nil)
        }
    }
    
    override func title(for section: Int) -> String {
        return "Business"
    }
}
