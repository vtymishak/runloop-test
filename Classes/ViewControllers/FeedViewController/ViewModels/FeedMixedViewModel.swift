//
//  FeedMixedViewModel.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import Foundation

class FeedMixedViewModel: FeedViewModel {
    // MARK: - Properties
    fileprivate let titles = ["Entertainment", "Environment"]
    
    fileprivate var isEntertainmentLoading: Bool {
        didSet {
            isLoadingInProgress = isEntertainmentLoading || isEnvironmentLoading
        }
    }
    fileprivate var isEnvironmentLoading: Bool {
        didSet {
            isLoadingInProgress = isEntertainmentLoading || isEnvironmentLoading
        }
    }
    
    // MARK: - Init
    override init() {
        isEntertainmentLoading = false
        isEnvironmentLoading = false
        super.init()
        feed = [[FeedItem](), [FeedItem]()]
    }
    
    // MARK: - Public
    override func request() {
        requestEntertainmentFeed()
        requestEnvironmentFeed()
    }
    
    override func title(for section: Int) -> String {
        guard titles.count > section else { return "" }
        return titles[section]
    }
    
    // MARK: - Helpers
    fileprivate func requestEntertainmentFeed() {
        isEntertainmentLoading = true
        API.requestEntertainmentFeed { [weak self] (items, error) in
            guard let `self` = self else { return }
            self.isEntertainmentLoading = false
            guard error == nil else {
                self.feedUpdateCallback?(nil, error!)
                return
            }
            guard let items = items else {
                self.feedUpdateCallback?(nil, ServerError.common)
                return
            }
            self.feed[0] = items
            self.feedUpdateCallback?(self.feed, nil)
        }
    }
    
    fileprivate func requestEnvironmentFeed() {
        isEnvironmentLoading = true
        API.requestEnvironmentFeed { [weak self] (items, error) in
            guard let `self` = self else { return }
            self.isEnvironmentLoading = false
            guard error == nil else {
                self.feedUpdateCallback?(nil, error!)
                return
            }
            guard let items = items else {
                self.feedUpdateCallback?(nil, ServerError.common)
                return
            }
            self.feed[1] = items
            self.feedUpdateCallback?(self.feed, nil)
        }
    }
}
