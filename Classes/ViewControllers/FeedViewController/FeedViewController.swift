//
//  FeedViewController.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit

class FeedViewController: BaseTableViewController {
    // MARK: - Properties
    override var navigationTitle: String {
        return "Feed"
    }
    
    fileprivate var viewModel: FeedViewModel?
    fileprivate var businessViewModel = FeedBusinessViewModel()
    fileprivate var mixedViewModel = FeedMixedViewModel()
    
    fileprivate var isLoadingInProgress: Bool {
        return businessViewModel.isLoadingInProgress || mixedViewModel.isLoadingInProgress
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViewModel(with: .business)
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // reset selected feed item
        NotificationCenter.default.post(name: .FeedItemSelected, object: nil)
    }
    
    // MARK: - Helpers
    fileprivate func updateViewModel(with tab: FeedTab) {
        switch tab {
        case .business:
            viewModel = businessViewModel
        case .mixed:
            viewModel = mixedViewModel
        }
        if viewModel!.hasLoadedItems {
            tableView.reloadData()
        } else {
            viewModel?.request()
        }
    }
    
    fileprivate func bind() {
        businessViewModel.registerChangeLoadingStateCallback { [unowned self] (viewModel) in
            if self.isLoadingInProgress {
                self.showLoading()
            } else {
                self.hideLoading()
            }
        }
        mixedViewModel.registerChangeLoadingStateCallback { [unowned self] (viewModel) in
            if self.isLoadingInProgress {
                self.showLoading()
            } else {
                self.hideLoading()
            }
        }
        businessViewModel.registerFeedUpdateCallback({ [unowned self] (items, error) in
            self.updateContent(items, error: error)
        })
        mixedViewModel.registerFeedUpdateCallback({ [unowned self] (items, error) in
            self.updateContent(items, error: error)
        })
    }
    
    fileprivate func updateContent(_ items: [[FeedItem]]?, error: ServerError?) {
        hideLoading()
        guard error == nil else {
            showError(with: error!.reason)
            return
        }
        tableView.reloadData()
    }
    
    override func setupTableView() {
        super.setupTableView()
        
        let headerView = FeedHeaderView(frame: CGRect(origin: .zero, size: CGSize(width: view.bounds.width, height: 70)))
        headerView.backgroundColor = .white
        headerView.delegate = self
        
        tableView.tableHeaderView = headerView
        tableView.backgroundColor = .white
    }
    
    override func registerCells() {
        super.registerCells()
        tableView.register(FeedItemCell.self, forCellReuseIdentifier: FeedItemCell.reuseIdentifier)
    }
}

// MARK: - FeedViewController: FeedHeaderViewDelegate -
extension FeedViewController: FeedHeaderViewDelegate {
    func onHeaderView(_ headerView: FeedHeaderView, changedTab tab: FeedTab) {
        updateViewModel(with: tab)
    }
}

// MARK: - FeedViewController: UITableViewDataSource -
extension FeedViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows(in: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedItemCell.reuseIdentifier, for: indexPath)
        if let feedCell = cell as? FeedItemCell {
            feedCell.feedItem = viewModel?.feedItem(at: indexPath)
        }
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections ?? 0
    }
}

// MARK: - FeedViewController: UITableViewDelegate -
extension FeedViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard let cell = tableView.cellForRow(at: indexPath) as? FeedItemCell else { return }
        guard let feedItem = cell.feedItem else { return }
        navigationController?.pushViewController(FeedItemDetailViewController(with: feedItem), animated: true)
        NotificationCenter.default.post(name: .FeedItemSelected, object: feedItem)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel?.title(for: section)
    }
}
