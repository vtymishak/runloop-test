//
//  FeedItemCell.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit

class FeedItemCell: UITableViewCell {
    // MARK: - Properties
    var feedItem: FeedItem? {
        didSet {
            updateContent()
        }
    }
    
    fileprivate var feedItemTitleLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = UIColor.appBlack
        label.font = UIFont.appThinFont(ofSize: 18)
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        return label
    }()
    
    // MARK: - Init
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        attachUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    private func attachUI() {
        addSubview(feedItemTitleLabel)
    }
    
    private func layoutUI() {
        feedItemTitleLabel.frame = CGRect(
            x: 20,
            y: bounds.midY - 10,
            width: bounds.width - 40,
            height: 20
        )
    }
    
    // MARK: - Update
    private func updateContent() {
        guard let feedItem = feedItem else { return }
        feedItemTitleLabel.text = feedItem.title
    }
}
