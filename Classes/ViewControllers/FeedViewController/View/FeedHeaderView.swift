//
//  FeedHeaderView.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit

enum FeedTab: Int {
    case business = 0
    case mixed
}

class FeedHeaderView: UIView {
    // MARK: - Properties
    weak var delegate: FeedHeaderViewDelegate?
    
    fileprivate var segmentedControl: UISegmentedControl = {
        var control = UISegmentedControl(items: ["Business", "Mixed"])
        control.selectedSegmentIndex = 0
        control.addTarget(self, action: #selector(onSegmentedControlValueChanged(_:)), for: .valueChanged)
        return control
    }()
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        attachUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    private func attachUI() {
        addSubview(segmentedControl)
    }
    
    private func layoutUI() {
        segmentedControl.frame = CGRect(
            x: 20,
            y: 20,
            width: bounds.width - 40,
            height: 30
        )
    }
    
    // MARK: - Callbacks
    @objc private func onSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        guard let tab = FeedTab(rawValue: sender.selectedSegmentIndex) else { return }
        delegate?.onHeaderView(self, changedTab: tab)
    }
}

// MARK: - FeedHeaderViewDelegate -
protocol FeedHeaderViewDelegate: class {
    func onHeaderView(_ headerView: FeedHeaderView, changedTab tab: FeedTab)
}
