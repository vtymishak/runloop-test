//
//  InfoView.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit

class InfoView: UIView {
    // MARK: - Properties
    fileprivate var nameLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.appBlack
        label.font = UIFont.appBoldFont(ofSize: 32)
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.text = "Vladimir Tymishak"
        return label
    }()
    
    fileprivate var dateLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.appBlack
        label.font = UIFont.appThinFont(ofSize: 18)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    fileprivate var feedItemTitleLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.appBlack
        label.font = UIFont.appRegularFont(ofSize: 18)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        attachUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    private func attachUI() {
        addSubview(nameLabel)
        addSubview(dateLabel)
        addSubview(feedItemTitleLabel)
    }
    
    private func layoutUI() {
        nameLabel.frame = CGRect(
            x: 20,
            y: 20,
            width: bounds.width - 40,
            height: 40
        )
        dateLabel.frame = CGRect(
            x: 20,
            y: nameLabel.frame.maxY + 20,
            width: bounds.width - 40,
            height: 60
        )
        feedItemTitleLabel.frame = CGRect(
            x: 20,
            y: dateLabel.frame.maxY + 20,
            width: bounds.width - 40,
            height: 200
        )
    }
    
    // MARK: - Public
    func updateDateLabel(with date: Date) {
        DispatchQueue.main.async {
            self.dateLabel.text = date.defaultFormattedString
        }
    }
    
    func updateFeedItemLabel(with title: String?) {
        DispatchQueue.main.async {
            self.feedItemTitleLabel.text = title
        }
    }
}
