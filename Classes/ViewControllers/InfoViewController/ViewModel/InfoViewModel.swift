//
//  InfoViewModel.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import Foundation

typealias DateChangedCallback = (Date) -> Void
typealias FeedItemSelectedCallback = (String) -> Void

class InfoViewModel {
    // MARK: - Properties
    fileprivate var dateChanged: DateChangedCallback?
    fileprivate var feedItemSelected: FeedItemSelectedCallback?
    
    fileprivate var dateTimer: Timer?
    
    init() {
        setupNotificationListener()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Public
    func registerDateChangingCallback(_ callback: @escaping DateChangedCallback) {
        dateChanged = callback
        startDateTimer()
    }
    
    func registerNewsSelectedCallback(_ callback: @escaping FeedItemSelectedCallback) {
        feedItemSelected = callback
    }
    
    // MARK: - Helpers
    fileprivate func setupNotificationListener() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onFeedItemSelected(_:)),
            name: .FeedItemSelected,
            object: nil
        )
    }
    
    fileprivate func startDateTimer() {
        dateTimer?.invalidate()
        dateTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [unowned self] (_) in
            self.dateChanged?(Date())
        }
    }
    
    @objc func onFeedItemSelected(_ notification: Notification) {
        guard let feedItem = notification.object as? FeedItem else {
            feedItemSelected?("")
            return
        }
        let title = feedItem.title
        feedItemSelected?(title)
    }
}
