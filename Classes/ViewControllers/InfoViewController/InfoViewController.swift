//
//  InfoViewController.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit

class InfoViewController: BaseViewController {
    // MARK: - Properties
    override var navigationTitle: String {
        return "Info"
    }
    
    fileprivate var infoView: InfoView!
    
    fileprivate var viewModel = InfoViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInfoView()
        bind()
    }
    
    private func setupInfoView() {
        infoView = InfoView(frame: view.bounds)
        infoView.backgroundColor = .white
        infoView.updateDateLabel(with: Date())
        view.addSubview(infoView)
    }
    
    private func bind() {
        viewModel.registerDateChangingCallback { [unowned self] (newDate) in
            self.infoView.updateDateLabel(with: newDate)
        }
        viewModel.registerNewsSelectedCallback { [unowned self] (feedItemTitle) in
            self.infoView.updateFeedItemLabel(with: feedItemTitle)
        }
    }
}
