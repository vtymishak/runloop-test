//
//  TabBarController.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewControllers()
    }
    
    private func setupViewControllers() {
        let meta = [
            ["title": "Info", "controller": InfoViewController()],
            ["title": "Feed", "controller": FeedViewController()]
        ]
        var controllers = [UIViewController]()
        for i in 0..<meta.count {
            let info = meta[i]
            
            guard let controller = info["controller"] as? UIViewController else { continue }
            controllers.append(UINavigationController(rootViewController: controller))
        }
        viewControllers = controllers
        for i in 0..<meta.count {
            let info = meta[i]
            
            guard let title = info["title"] as? String else { continue }
            tabBar.items![i].title = title
            tabBar.items![i].titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -18)
        }
    }
}
