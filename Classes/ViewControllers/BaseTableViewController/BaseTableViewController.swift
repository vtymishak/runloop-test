//
//  BaseTableViewController.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit

class BaseTableViewController: BaseViewController {
    // MARK: - Properties
    let tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .plain)
        table.backgroundColor = .clear
        table.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        table.separatorStyle = .none
        return table
    }()
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        registerCells()
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.frame = view.bounds
        tableView.backgroundColor = .clear
        tableView.keyboardDismissMode = .onDrag
        view.addSubview(tableView)
    }
    
    func registerCells() {
        
    }
}

// MARK: - BaseTableViewController: UITableViewDataSource -
extension BaseTableViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

// MARK: - BaseTableViewController: UITableViewDelegate -
extension BaseTableViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 0
    }
}
