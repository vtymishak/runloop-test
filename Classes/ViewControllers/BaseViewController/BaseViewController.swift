//
//  BaseViewController.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit
import SwiftMessages

class BaseViewController: UIViewController {
    // MARK: - Properties
    var navigationTitle: String {
        return ""
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
    
    // MARK: - Helpers
    func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.title = navigationTitle
    }
    
    func showError(with message: String) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.backgroundView.backgroundColor = .red
        view.bodyLabel?.textColor = .white
        view.button?.frame = .zero
        view.button?.isHidden = true
        view.configureContent(title: "Error occured", body: message)
        
        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        
        DispatchQueue.main.async {
            SwiftMessages.show(config: config, view: view)
        }
    }
    
    func showLoading() {
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.backgroundView.backgroundColor = .orange
        view.bodyLabel?.textColor = .white
        view.button?.frame = .zero
        view.button?.isHidden = true
        view.configureContent(body: "Refreshing...")
        
        var config = SwiftMessages.defaultConfig
        config.duration = .forever
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        
        DispatchQueue.main.async {
            SwiftMessages.show(config: config, view: view)
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            SwiftMessages.hideAll()
        }
    }
}
