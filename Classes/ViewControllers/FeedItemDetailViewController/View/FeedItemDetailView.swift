//
//  FeedItemDetailView.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit

class FeedItemDetailView: UIView {
    // MARK: - Properties
    var feedItem: FeedItem? {
        didSet {
            updateContent()
        }
    }
    
    fileprivate var feedItemDescriptionLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = UIColor.appBlack
        label.font = UIFont.appThinFont(ofSize: 18)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        attachUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    private func attachUI() {
        addSubview(feedItemDescriptionLabel)
    }
    
    private func layoutUI() {
        feedItemDescriptionLabel.frame = CGRect(
            x: 20,
            y: 20,
            width: bounds.width - 40,
            height: bounds.height - 40
        )
    }
    
    // MARK: - Update
    private func updateContent() {
        guard let feedItem = feedItem else { return }
        feedItemDescriptionLabel.text = feedItem.description
    }
    
    // MARK: - Public
    static func height(for feedItem: FeedItem?) -> CGFloat {
        guard let feedItem = feedItem else { return 0 }
        var height = feedItem.description.size(
            with: UIFont.appThinFont(ofSize: 18),
            constrainedToWidth: UIScreen.main.bounds.width - 40
        ).height
        height += CGFloat(40)
        return height
    }
}
