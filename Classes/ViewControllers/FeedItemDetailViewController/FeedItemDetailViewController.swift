//
//  FeedItemDetailViewController.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import UIKit

class FeedItemDetailViewController: BaseViewController {
    // MARK: - Properties
    override var navigationTitle: String {
        return "Item details"
    }
    
    fileprivate var feedItem: FeedItem?
    
    // MARK: - Init
    convenience init(with feedItem: FeedItem) {
        self.init(nibName: nil, bundle: nil)
        self.feedItem = feedItem
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDescriptionView()
    }
    
    // MARK: - Helpers
    fileprivate func setupDescriptionView() {
        guard let feedItem = feedItem else { return }
        let height = FeedItemDetailView.height(for: feedItem)
        let rect = CGRect(x: 0, y: 0, width: view.bounds.width, height: height)
        let descriptionView = FeedItemDetailView(frame: rect)
        descriptionView.feedItem = feedItem
        
        let tabBarHeight = tabBarController?.tabBar.bounds.height ?? 0
        let scrollViewRect = CGRect(
            origin: .zero,
            size: CGSize(width: view.bounds.width, height: view.bounds.height - tabBarHeight)
        )
        let scrollView = UIScrollView(frame: scrollViewRect)
        scrollView.contentSize = descriptionView.bounds.size
        
        scrollView.addSubview(descriptionView)
        view.addSubview(scrollView)
        view.backgroundColor = .white
    }
    
}
