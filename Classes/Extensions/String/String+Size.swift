//
//  String+Size.swift
//  StarShot
//
//  Created by Anatoliy Radchenko on 01/05/2017.
//  Copyright © 2017 StarShot. All rights reserved.
//

import UIKit

extension String {
    func size(with font: UIFont, constrainedToWidth width: CGFloat = .greatestFiniteMagnitude) -> CGSize {
        let paragraph = NSMutableParagraphStyle()
        paragraph.setParagraphStyle(NSParagraphStyle.default)
        paragraph.lineBreakMode = .byWordWrapping
        let attrString = NSAttributedString(string: self, attributes: [
            NSAttributedStringKey.font : font,
            NSAttributedStringKey.paragraphStyle: paragraph
            ])
        
        let sizeConstraint = CGSize(width: width, height: .greatestFiniteMagnitude)
        
        let framesetter = CTFramesetterCreateWithAttributedString(attrString)
        return CTFramesetterSuggestFrameSizeWithConstraints(
            framesetter,
            CFRange(location: 0, length: 0),
            nil,
            sizeConstraint,
            nil
        )
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
}

extension NSAttributedString {
    func size(constrainedToWidth width: CGFloat = .greatestFiniteMagnitude) -> CGSize {
        let sizeConstraint = CGSize(width: width, height: .greatestFiniteMagnitude)
        let framesetter = CTFramesetterCreateWithAttributedString(self)
        return CTFramesetterSuggestFrameSizeWithConstraints(
            framesetter,
            CFRange(location: 0, length: 0),
            nil,
            sizeConstraint,
            nil
        )
    }
}
