//
//  Date+FormattedString.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import Foundation

extension Date {
    var defaultFormattedString: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY/M/d\nhh:mm:ssa"
        let result = formatter.string(from: self)
        return result
    }
}
