//
//  UIFont+Application.swift
//  PRG
//
//  Created by Vladimir Tymishak on 24.10.17.
//  Copyright © 2017 own. All rights reserved.
//

import UIKit

extension UIFont {
    static func appRegularFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Medium", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func appThinFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Light", size: size) ?? UIFont.systemFont(ofSize: size, weight: .thin)
    }
    
    static func appBoldFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Black", size: size) ?? UIFont.systemFont(ofSize: size, weight: .bold)
    }
}
