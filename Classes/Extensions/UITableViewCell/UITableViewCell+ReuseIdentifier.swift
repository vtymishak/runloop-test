//
//  UITableViewCell+ReuseIdentifier.swift
//  PRG
//
//  Created by Vladimir Tymishak on 25.10.17.
//  Copyright © 2017 own. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var reuseIdentifier: String {
        return NSStringFromClass(self)
    }
}
