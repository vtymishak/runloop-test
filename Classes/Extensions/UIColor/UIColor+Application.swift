//
//  UIColor+Application.swift
//  PRG
//
//  Created by Vladimir Tymishak on 24.10.17.
//  Copyright © 2017 own. All rights reserved.
//

import UIKit

extension UIColor {
    static var appBlack: UIColor {
        return #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 1)
    }
    
    static var appGrayBlack: UIColor {
        return #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)
    }
    
    static var appOrange: UIColor {
        return #colorLiteral(red: 0.9960784314, green: 0.8156862745, blue: 0.6274509804, alpha: 1)
    }
    
    static var appLightOrange: UIColor {
        return #colorLiteral(red: 0.9764705882, green: 0.9254901961, blue: 0.8666666667, alpha: 1)
    }
    
    static var appGray: UIColor {
        return #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
    }
    
    static var appLightGray: UIColor {
        return #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
    }
    
    static var appWhite: UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    static var facebookBlue: UIColor {
        return #colorLiteral(red: 0.2392156863, green: 0.3529411765, blue: 0.5960784314, alpha: 1)
    }
}
