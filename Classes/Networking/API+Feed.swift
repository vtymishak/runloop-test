//
//  API+Feed.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import Foundation
import SWXMLHash
import Alamofire

typealias FeedRequestCompletion = ([FeedItem]?, ServerError?) -> Void

extension API {
    static func requestBusinessFeed(with completion: @escaping FeedRequestCompletion) {
        API.get(from: "businessNews", with: nil) { (response, error) in
            let (items, err) = API.processResponse(response, error)
            completion(items, err)
        }
    }
    
    static func requestEntertainmentFeed(with completion: @escaping FeedRequestCompletion) {
        API.get(from: "entertainment", with: nil) { (response, error) in
            let (items, err) = API.processResponse(response, error)
            completion(items, err)
        }
    }
    
    static func requestEnvironmentFeed(with completion: @escaping FeedRequestCompletion) {
        API.get(from: "environment", with: nil) { (response, error) in
            let (items, err) = API.processResponse(response, error)
            completion(items, err)
        }
    }
    
    private static func processResponse(_ response: DefaultDataResponse, _ error: ServerError?) -> ([FeedItem]?, ServerError?) {
        guard error == nil else {
            return (nil, ServerError.fail(reason: error!.localizedDescription))
        }
        guard let data = response.data else {
            return (nil, ServerError.common)
        }
        let feed = SWXMLHash.parse(data)
        do {
            let items: [FeedItem] = try feed["rss"]["channel"]["item"].value()
            return (items, nil)
        } catch (let error) {
            return (nil, ServerError.fail(reason: error.localizedDescription))
        }
    }
}
