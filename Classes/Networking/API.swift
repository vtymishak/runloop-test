//
//  API.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import Alamofire

// MARK: - ServerError -
enum ServerError: Error {
    case fail(reason: String)
    
    static var common: ServerError {
        return ServerError.fail(reason: "Common error")
    }
    
    var reason: String {
        switch self {
        case .fail(let reason):
            return reason
        }
    }
}

// MARK: - API -
class API {
    // MARK: - Properties
    typealias RequestCompletion = (DefaultDataResponse, ServerError?) -> Void
    
    static let baseUrl: String = "http://feeds.reuters.com/reuters/"
    
    fileprivate static var headers: Dictionary = ["Accept": "application/xml"]
    
    fileprivate static let reachabilityManager = NetworkReachabilityManager()
    
    static var isReachable: Bool {
        return reachabilityManager?.isReachable ?? false
    }
    
    // MARK: - Auth management
    static func setHeader(_ value: String?, for key: String) {
        if let value = value {
            API.headers[key] = value
        } else {
            API.headers.removeValue(forKey: key)
        }
    }
    
    // MARK: - Requests
    static func get(from url: String, with params: [String: Any]?, _ completion: @escaping RequestCompletion) {
        guard API.isReachable else { return }
        let urlString = baseUrl + url
        Alamofire.request(
            urlString,
            method: .get,
            parameters: params,
            encoding: URLEncoding.queryString,
            headers: API.headers
            ).response { (response) in
                processResponse(response, with: completion)
        }
    }
    
    // MARK: - Response parsing
    fileprivate static func processResponse(_ dataResponse: DefaultDataResponse, with completion: RequestCompletion) {
        guard let response = dataResponse.response else {
            print("[API] Failed to get HTTP response")
            completion(dataResponse, ServerError.common)
            return
        }
        if response.statusCode == 200 {
            completion(dataResponse, nil)
        } else {
            guard let error = dataResponse.error else {
                completion(dataResponse, ServerError.common)
                return
            }
            completion(dataResponse, ServerError.fail(reason: error.localizedDescription))
        }
    }
}
