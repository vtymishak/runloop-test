//
//  FeedItem.swift
//  Runloop Test
//
//  Created by Vladimir Tymishak on 09.11.17.
//  Copyright © 2017 task. All rights reserved.
//

import Foundation
import SWXMLHash

final class FeedItem {
    var title: String = ""
    var description: String = ""
    
    init(title: String?, description: String?) {
        self.title = title ?? ""
        self.description = description ?? ""
        
        // remove HTML tags
        self.description = self.description.replacingOccurrences(
            of: "<[^>]+>",
            with: "",
            options: .regularExpression,
            range: nil
        )
    }
}

extension FeedItem: XMLIndexerDeserializable {
    static func deserialize(_ node: XMLIndexer) throws -> FeedItem {
        return try FeedItem(
            title: node["title"].value(),
            description: node["description"].value()
        )
    }
}
